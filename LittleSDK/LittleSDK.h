//
//  LittleSDK.h
//  LittleSDK
//
//  Created by Gabriel John on 29/01/2022.
//

#import <Foundation/Foundation.h>

#import <LittleSDK/CryptLib.h>
#import <LittleSDK/NSData+Base64.h>

//! Project version number for LittleSDK.
FOUNDATION_EXPORT double LittleSDKVersionNumber;

//! Project version string for LittleSDK.
FOUNDATION_EXPORT const unsigned char LittleSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <LittleSDK/PublicHeader.h>

