//
//  CryptLib.h
//


#import <CommonCrypto/CommonDigest.h>
#import <CommonCrypto/CommonCryptor.h>
#include <UIKit/UIKit.h>

@interface StringEncryptionSDK : NSObject

-  (NSData *)encrypt:(NSData *)plainText key:(NSString *)key iv:(NSString *)iv;
-  (NSData *)decrypt:(NSData *)encryptedText key:(NSString *)key iv:(NSString *)iv;
-  (NSData *)encrypt2:(NSData *)plainText key:(NSString *)key iv:(NSString *)iv;
-  (NSData *)decrypt2:(NSData *)encryptedText key:(NSString *)key iv:(NSString *)iv;

-  (NSData *)generateRandomIV:(size_t)length;
-  (NSString *) md5:(NSString *) input;
-  (NSString*) hex: (NSData *) data;
-  (NSString*) sha256:(NSString *)key length:(NSInteger) length;

@end
